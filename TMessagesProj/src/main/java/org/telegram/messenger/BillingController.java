package org.telegram.messenger;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
<<<<<<< HEAD
=======
import androidx.core.util.Pair;
>>>>>>> neko/main

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ProductDetails;
import com.android.billingclient.api.ProductDetailsResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.QueryProductDetailsParams;
import com.android.billingclient.api.QueryPurchasesParams;
<<<<<<< HEAD
import com.google.android.exoplayer2.util.Util;

import org.json.JSONObject;
=======

import org.telegram.messenger.utils.BillingUtilities;
>>>>>>> neko/main
import org.telegram.tgnet.ConnectionsManager;
import org.telegram.tgnet.TLRPC;
import org.telegram.ui.PremiumPreviewFragment;

<<<<<<< HEAD
import java.io.InputStream;
=======
>>>>>>> neko/main
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
<<<<<<< HEAD
import java.util.Iterator;
=======
>>>>>>> neko/main
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class BillingController implements PurchasesUpdatedListener, BillingClientStateListener {
    public final static String PREMIUM_PRODUCT_ID = "telegram_premium";
    public final static QueryProductDetailsParams.Product PREMIUM_PRODUCT = QueryProductDetailsParams.Product.newBuilder()
            .setProductType(BillingClient.ProductType.SUBS)
            .setProductId(PREMIUM_PRODUCT_ID)
            .build();

    @Nullable
    public static ProductDetails PREMIUM_PRODUCT_DETAILS;

    private static BillingController instance;

    public static boolean billingClientEmpty;

<<<<<<< HEAD
    private Map<String, Consumer<BillingResult>> resultListeners = new HashMap<>();
    private List<String> requestingTokens = new ArrayList<>();
    private String lastPremiumTransaction;
    private String lastPremiumToken;

    private Map<String, Integer> currencyExpMap = new HashMap<>();
=======
    private final Map<String, Consumer<BillingResult>> resultListeners = new HashMap<>();
    private final List<String> requestingTokens = Collections.synchronizedList(new ArrayList<>());
    private final Map<String, Integer> currencyExpMap = new HashMap<>();
    private final BillingClient billingClient;
    private String lastPremiumTransaction;
    private String lastPremiumToken;
    private boolean isDisconnected;
>>>>>>> neko/main

    public static BillingController getInstance() {
        if (instance == null) {
            instance = new BillingController(ApplicationLoader.applicationContext);
        }
        return instance;
    }

<<<<<<< HEAD
    private BillingClient billingClient;

=======
>>>>>>> neko/main
    private BillingController(Context ctx) {
        billingClient = BillingClient.newBuilder(ctx)
                .enablePendingPurchases()
                .setListener(this)
                .build();
    }

    public String getLastPremiumTransaction() {
        return lastPremiumTransaction;
    }

    public String getLastPremiumToken() {
        return lastPremiumToken;
    }

    public String formatCurrency(long amount, String currency) {
        return formatCurrency(amount, currency, getCurrencyExp(currency));
    }

    public String formatCurrency(long amount, String currency, int exp) {
        if (currency.isEmpty()) {
            return String.valueOf(amount);
        }
        Currency cur = Currency.getInstance(currency);
        if (cur != null) {
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
            numberFormat.setCurrency(cur);
<<<<<<< HEAD

=======
>>>>>>> neko/main
            return numberFormat.format(amount / Math.pow(10, exp));
        }
        return amount + " " + currency;
    }

<<<<<<< HEAD
    public int getCurrencyExp(String currency) {
        Integer exp = currencyExpMap.get(currency);
        if (exp == null) {
            return 0;
        }
        return exp;
=======
    @SuppressWarnings("ConstantConditions")
    public int getCurrencyExp(String currency) {
        BillingUtilities.extractCurrencyExp(currencyExpMap);
        return currencyExpMap.getOrDefault(currency, 0);
>>>>>>> neko/main
    }

    public void startConnection() {
        if (isReady()) {
            return;
        }
<<<<<<< HEAD
        try {
            Context ctx = ApplicationLoader.applicationContext;
            InputStream in = ctx.getAssets().open("currencies.json");
            JSONObject obj = new JSONObject(new String(Util.toByteArray(in), "UTF-8"));
            parseCurrencies(obj);
            in.close();
        } catch (Exception e) {
            FileLog.e(e);
        }

=======
        BillingUtilities.extractCurrencyExp(currencyExpMap);
>>>>>>> neko/main
        if (!BuildVars.useInvoiceBilling()) {
            billingClient.startConnection(this);
        }
    }

    private void switchToInvoice() {
        if (billingClientEmpty) {
            return;
        }
        billingClientEmpty = true;
        NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.billingProductDetailsUpdated);
    }

<<<<<<< HEAD
    private void parseCurrencies(JSONObject obj) {
        Iterator<String> it = obj.keys();
        while (it.hasNext()) {
            String key = it.next();
            JSONObject currency = obj.optJSONObject(key);
            currencyExpMap.put(key, currency.optInt("exp"));
        }
    }

=======
>>>>>>> neko/main
    public boolean isReady() {
        return billingClient.isReady();
    }

    public void queryProductDetails(List<QueryProductDetailsParams.Product> products, ProductDetailsResponseListener responseListener) {
        if (!isReady()) {
<<<<<<< HEAD
            throw new IllegalStateException("Billing controller should be ready for this call!");
=======
            throw new IllegalStateException("Billing: Controller should be ready for this call!");
>>>>>>> neko/main
        }
        billingClient.queryProductDetailsAsync(QueryProductDetailsParams.newBuilder().setProductList(products).build(), responseListener);
    }

<<<<<<< HEAD
=======
    /**
     * {@link BillingClient#queryPurchasesAsync} returns only active subscriptions and not consumed purchases.
     */
>>>>>>> neko/main
    public void queryPurchases(String productType, PurchasesResponseListener responseListener) {
        billingClient.queryPurchasesAsync(QueryPurchasesParams.newBuilder().setProductType(productType).build(), responseListener);
    }

    public boolean startManageSubscription(Context ctx, String productId) {
        try {
            ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://play.google.com/store/account/subscriptions?sku=%s&package=%s", productId, ctx.getPackageName()))));
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public void addResultListener(String productId, Consumer<BillingResult> listener) {
        resultListeners.put(productId, listener);
    }

    public void launchBillingFlow(Activity activity, AccountInstance accountInstance, TLRPC.InputStorePaymentPurpose paymentPurpose, List<BillingFlowParams.ProductDetailsParams> productDetails) {
        launchBillingFlow(activity, accountInstance, paymentPurpose, productDetails, null, false);
    }

    public void launchBillingFlow(Activity activity, AccountInstance accountInstance, TLRPC.InputStorePaymentPurpose paymentPurpose, List<BillingFlowParams.ProductDetailsParams> productDetails, BillingFlowParams.SubscriptionUpdateParams subscriptionUpdateParams, boolean checkedConsume) {
        if (!isReady() || activity == null) {
            return;
        }

        if (paymentPurpose instanceof TLRPC.TL_inputStorePaymentGiftPremium && !checkedConsume) {
            queryPurchases(BillingClient.ProductType.INAPP, (billingResult, list) -> {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Runnable callback = () -> launchBillingFlow(activity, accountInstance, paymentPurpose, productDetails, subscriptionUpdateParams, true);

                    AtomicInteger productsToBeConsumed = new AtomicInteger(0);
                    List<String> productsConsumed = new ArrayList<>();
                    for (Purchase purchase : list) {
                        if (purchase.isAcknowledged()) {
                            for (BillingFlowParams.ProductDetailsParams params : productDetails) {
                                String productId = params.zza().getProductId();
                                if (purchase.getProducts().contains(productId)) {
                                    productsToBeConsumed.incrementAndGet();
                                    billingClient.consumeAsync(ConsumeParams.newBuilder()
                                                    .setPurchaseToken(purchase.getPurchaseToken())
                                            .build(), (billingResult1, s) -> {
                                        if (billingResult1.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                                            productsConsumed.add(productId);

                                            if (productsToBeConsumed.get() == productsConsumed.size()) {
                                                callback.run();
                                            }
                                        }
                                    });
                                    break;
                                }
                            }
                        } else {
                            onPurchasesUpdated(BillingResult.newBuilder().setResponseCode(BillingClient.BillingResponseCode.OK).build(), Collections.singletonList(purchase));
                            return;
                        }
                    }

                    if (productsToBeConsumed.get() == 0) {
                        callback.run();
                    }
                }
            });
            return;
        }

<<<<<<< HEAD
        BillingFlowParams.Builder flowParams = BillingFlowParams.newBuilder()
=======
        Pair<String, String> payload = BillingUtilities.createDeveloperPayload(paymentPurpose, accountInstance);
        String obfuscatedAccountId = payload.first;
        String obfuscatedData = payload.second;

        BillingFlowParams.Builder flowParams = BillingFlowParams.newBuilder()
                .setObfuscatedAccountId(obfuscatedAccountId)
                .setObfuscatedProfileId(obfuscatedData)
>>>>>>> neko/main
                .setProductDetailsParamsList(productDetails);
        if (subscriptionUpdateParams != null) {
            flowParams.setSubscriptionUpdateParams(subscriptionUpdateParams);
        }
<<<<<<< HEAD
        boolean ok = billingClient.launchBillingFlow(activity, flowParams.build()).getResponseCode() == BillingClient.BillingResponseCode.OK;

        if (ok) {
            for (BillingFlowParams.ProductDetailsParams params : productDetails) {
                accountInstance.getUserConfig().billingPaymentPurpose = paymentPurpose;
                accountInstance.getUserConfig().awaitBillingProductIds.add(params.zza().getProductId()); // params.getProductDetails().getProductId()
            }
            accountInstance.getUserConfig().saveConfig(false);
=======
        int responseCode = billingClient.launchBillingFlow(activity, flowParams.build()).getResponseCode();
        if (responseCode != BillingClient.BillingResponseCode.OK) {
            FileLog.d("Billing: Launch Error: " + responseCode + ", " + obfuscatedAccountId + ", " + obfuscatedData);
>>>>>>> neko/main
        }
    }

    @Override
<<<<<<< HEAD
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
        FileLog.d("Billing purchases updated: " + billingResult + ", " + list);
        if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                PremiumPreviewFragment.sentPremiumBuyCanceled();
            }

            for (int i = 0; i < UserConfig.MAX_ACCOUNT_COUNT; i++) {
                AccountInstance acc = AccountInstance.getInstance(i);
                if (!acc.getUserConfig().awaitBillingProductIds.isEmpty()) {
                    acc.getUserConfig().awaitBillingProductIds.clear();
                    acc.getUserConfig().billingPaymentPurpose = null;
                    acc.getUserConfig().saveConfig(false);
                }
            }

            return;
        }
        if (list == null) {
=======
    public void onPurchasesUpdated(@NonNull BillingResult billing, @Nullable List<Purchase> list) {
        FileLog.d("Billing: Purchases updated: " + billing + ", " + list);
        if (billing.getResponseCode() != BillingClient.BillingResponseCode.OK) {
            if (billing.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                PremiumPreviewFragment.sentPremiumBuyCanceled();
            }
            return;
        }
        if (list == null || list.isEmpty()) {
>>>>>>> neko/main
            return;
        }
        lastPremiumTransaction = null;
        for (Purchase purchase : list) {
            if (purchase.getProducts().contains(PREMIUM_PRODUCT_ID)) {
                lastPremiumTransaction = purchase.getOrderId();
                lastPremiumToken = purchase.getPurchaseToken();
            }

<<<<<<< HEAD
            if (!requestingTokens.contains(purchase.getPurchaseToken())) {
                for (int i = 0; i < UserConfig.MAX_ACCOUNT_COUNT; i++) {
                    AccountInstance acc = AccountInstance.getInstance(i);
                    if (acc.getUserConfig().awaitBillingProductIds.containsAll(purchase.getProducts()) && purchase.getPurchaseState() != Purchase.PurchaseState.PENDING) {
                        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
                            if (!purchase.isAcknowledged()) {
                                requestingTokens.add(purchase.getPurchaseToken());
                                TLRPC.TL_payments_assignPlayMarketTransaction req = new TLRPC.TL_payments_assignPlayMarketTransaction();
                                req.receipt = new TLRPC.TL_dataJSON();
                                req.receipt.data = purchase.getOriginalJson();
                                req.purpose = acc.getUserConfig().billingPaymentPurpose;
                                acc.getConnectionsManager().sendRequest(req, (response, error) -> {
                                    if (response instanceof TLRPC.Updates) {
                                        acc.getMessagesController().processUpdates((TLRPC.Updates) response, false);
                                        requestingTokens.remove(purchase.getPurchaseToken());

                                        for (String productId : purchase.getProducts()) {
                                            Consumer<BillingResult> listener = resultListeners.remove(productId);
                                            if (listener != null) {
                                                listener.accept(billingResult);
                                            }
                                        }

                                        if (req.purpose instanceof TLRPC.TL_inputStorePaymentGiftPremium) {
                                            billingClient.consumeAsync(ConsumeParams.newBuilder()
                                                            .setPurchaseToken(purchase.getPurchaseToken())
                                                    .build(), (billingResult1, s) -> {});
                                        }
                                    }
                                    if (response != null || (ApplicationLoader.isNetworkOnline() && error != null && error.code != -1000)) {
                                        acc.getUserConfig().awaitBillingProductIds.removeAll(purchase.getProducts());
                                        acc.getUserConfig().saveConfig(false);
                                    }
                                }, ConnectionsManager.RequestFlagFailOnServerErrors | ConnectionsManager.RequestFlagInvokeAfter);
                            } else {
                                acc.getUserConfig().awaitBillingProductIds.removeAll(purchase.getProducts());
                                acc.getUserConfig().saveConfig(false);
                            }
                        } else {
                            acc.getUserConfig().awaitBillingProductIds.removeAll(purchase.getProducts());
                            acc.getUserConfig().saveConfig(false);
                        }
                    }
=======
            if (!requestingTokens.contains(purchase.getPurchaseToken()) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
                Pair<AccountInstance, TLRPC.InputStorePaymentPurpose> payload = BillingUtilities.extractDeveloperPayload(purchase);
                if (payload == null) {
                    continue;
                }
                if (!purchase.isAcknowledged()) {
                    requestingTokens.add(purchase.getPurchaseToken());

                    TLRPC.TL_payments_assignPlayMarketTransaction req = new TLRPC.TL_payments_assignPlayMarketTransaction();
                    req.receipt = new TLRPC.TL_dataJSON();
                    req.receipt.data = purchase.getOriginalJson();
                    req.purpose = payload.second;

                    AccountInstance acc = payload.first;
                    acc.getConnectionsManager().sendRequest(req, (response, error) -> {
                        requestingTokens.remove(purchase.getPurchaseToken());

                        if (response instanceof TLRPC.Updates) {
                            acc.getMessagesController().processUpdates((TLRPC.Updates) response, false);

                            for (String productId : purchase.getProducts()) {
                                Consumer<BillingResult> listener = resultListeners.remove(productId);
                                if (listener != null) {
                                    listener.accept(billing);
                                }
                            }

                            consumeGiftPurchase(purchase, req.purpose);
                        } else if (error != null) {
                            FileLog.d("Billing: Confirmation Error: " + error.code + " " + error.text);
                            NotificationCenter.getGlobalInstance().postNotificationNameOnUIThread(NotificationCenter.billingConfirmPurchaseError, req, error);
                        }
                    }, ConnectionsManager.RequestFlagFailOnServerErrors | ConnectionsManager.RequestFlagInvokeAfter);
                } else {
                    consumeGiftPurchase(purchase, payload.second);
>>>>>>> neko/main
                }
            }
        }
    }

<<<<<<< HEAD
    @Override
    public void onBillingServiceDisconnected() {
        FileLog.d("Billing service disconnected");
=======
    /**
     * All consumable purchases must be consumed. For us it is a gift.
     * Without confirmation the user will not be able to buy the product again.
     */
    private void consumeGiftPurchase(Purchase purchase, TLRPC.InputStorePaymentPurpose purpose) {
        if (purpose instanceof TLRPC.TL_inputStorePaymentGiftPremium) {
            billingClient.consumeAsync(
                    ConsumeParams.newBuilder()
                            .setPurchaseToken(purchase.getPurchaseToken())
                            .build(), (r, s) -> {
                    });
        }
    }

    /**
     * May occur in extremely rare cases.
     * For example when Google Play decides to update.
     */
    @SuppressWarnings("Convert2MethodRef")
    @Override
    public void onBillingServiceDisconnected() {
        FileLog.d("Billing: Service disconnected");
        int delay = isDisconnected ? 15000 : 5000;
        isDisconnected = true;
        AndroidUtilities.runOnUIThread(() -> startConnection(), delay);
>>>>>>> neko/main
    }

    @Override
    public void onBillingSetupFinished(@NonNull BillingResult setupBillingResult) {
<<<<<<< HEAD
        FileLog.d("Billing setup finished with result " + setupBillingResult);
        if (setupBillingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
            queryProductDetails(Collections.singletonList(PREMIUM_PRODUCT), (billingResult, list) -> {
                FileLog.d("Query product details finished " + billingResult + ", " + list);
=======
        FileLog.d("Billing: Setup finished with result " + setupBillingResult);
        if (setupBillingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
            isDisconnected = false;
            queryProductDetails(Collections.singletonList(PREMIUM_PRODUCT), (billingResult, list) -> {
                FileLog.d("Billing: Query product details finished " + billingResult + ", " + list);
>>>>>>> neko/main
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    for (ProductDetails details : list) {
                        if (details.getProductId().equals(PREMIUM_PRODUCT_ID)) {
                            PREMIUM_PRODUCT_DETAILS = details;
                        }
                    }
                    if (PREMIUM_PRODUCT_DETAILS == null) {
                        switchToInvoice();
                    } else {
<<<<<<< HEAD
                        AndroidUtilities.runOnUIThread(() -> NotificationCenter.getGlobalInstance().postNotificationName(NotificationCenter.billingProductDetailsUpdated));
=======
                        NotificationCenter.getGlobalInstance().postNotificationNameOnUIThread(NotificationCenter.billingProductDetailsUpdated);
>>>>>>> neko/main
                    }
                } else {
                    switchToInvoice();
                }
            });
<<<<<<< HEAD
            queryPurchases(BillingClient.ProductType.SUBS, this::onPurchasesUpdated);
        } else {
            switchToInvoice();
=======
            queryPurchases(BillingClient.ProductType.INAPP, this::onPurchasesUpdated);
            queryPurchases(BillingClient.ProductType.SUBS, this::onPurchasesUpdated);
        } else {
            if (!isDisconnected) {
                switchToInvoice();
            }
>>>>>>> neko/main
        }
    }
}
