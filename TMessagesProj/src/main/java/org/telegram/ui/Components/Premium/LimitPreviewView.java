package org.telegram.ui.Components.Premium;

<<<<<<< HEAD
=======
import static org.telegram.messenger.AndroidUtilities.dp;

>>>>>>> neko/main
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.math.MathUtils;

import org.telegram.messenger.AndroidUtilities;
import org.telegram.messenger.LocaleController;
import org.telegram.messenger.R;
import org.telegram.messenger.UserConfig;
import org.telegram.messenger.Utilities;
<<<<<<< HEAD
=======
import org.telegram.tgnet.TLRPC;
>>>>>>> neko/main
import org.telegram.ui.ActionBar.Theme;
import org.telegram.ui.Components.ColoredImageSpan;
import org.telegram.ui.Components.CubicBezierInterpolator;
import org.telegram.ui.Components.EmptyStubSpan;
import org.telegram.ui.Components.LayoutHelper;

import java.util.ArrayList;

public class LimitPreviewView extends LinearLayout {

<<<<<<< HEAD
=======
    private float percent;
    private final int premiumLimit;
    private int currentValue;
>>>>>>> neko/main
    public int gradientTotalHeight;
    boolean wasAnimation;
    CounterView limitIcon;

    boolean inc;
    float progress;
<<<<<<< HEAD
=======
    int width1;
>>>>>>> neko/main

    int icon;

    TextView premiumCount;
    TextView defaultCount;
    private float position;
    private View parentVideForGradient;
    PremiumGradient.PremiumGradientTools staticGradient;
    int gradientYOffset;
    boolean wasHaptic;
    boolean animationCanPlay = true;
<<<<<<< HEAD
    LinearLayout limitsContainer;
    private boolean premiumLocked;

    public LimitPreviewView(@NonNull Context context, int icon, int currentValue, int premiumLimit) {
        this(context, icon, currentValue, premiumLimit, .5f);
    }

    @SuppressLint("SetTextI18n")
    public LimitPreviewView(@NonNull Context context, int icon, int currentValue, int premiumLimit, float inputPercent) {
        super(context);
        final float percent = MathUtils.clamp(inputPercent, 0.1f, 0.9f);
        this.icon = icon;
=======
    FrameLayout limitsContainer;
    private boolean premiumLocked;
    private final TextView defaultText;
    private final TextView premiumText;
    private boolean isBoostsStyle;

    Theme.ResourcesProvider resourcesProvider;
    private boolean animateIncrease;
    private int animateIncreaseWidth;
    float limitIconRotation;
    public boolean isStatistic;


    public LimitPreviewView(@NonNull Context context, int icon, int currentValue, int premiumLimit, Theme.ResourcesProvider resourcesProvider) {
        this(context, icon, currentValue, premiumLimit, .5f, resourcesProvider);
    }

    @SuppressLint("SetTextI18n")
    public LimitPreviewView(@NonNull Context context, int icon, int currentValue, int premiumLimit, float inputPercent, Theme.ResourcesProvider resourcesProvider) {
        super(context);
        this.resourcesProvider = resourcesProvider;
        this.percent = MathUtils.clamp(inputPercent, 0.1f, 0.9f);
        this.icon = icon;
        this.currentValue = currentValue;
        this.premiumLimit = premiumLimit;
>>>>>>> neko/main
        setOrientation(VERTICAL);
        setClipChildren(false);
        setClipToPadding(false);
        if (icon != 0) {
<<<<<<< HEAD
            setPadding(0, AndroidUtilities.dp(16), 0, 0);
            limitIcon = new CounterView(context);

            setIconValue(currentValue);

            limitIcon.setPadding(AndroidUtilities.dp(24), AndroidUtilities.dp(6), AndroidUtilities.dp(24), AndroidUtilities.dp(14));
            addView(limitIcon, LayoutHelper.createLinear(LayoutHelper.WRAP_CONTENT, LayoutHelper.WRAP_CONTENT, 0, Gravity.LEFT));
        }
        limitsContainer = new LinearLayout(context) {
=======
            setPadding(0, dp(16), 0, 0);
            limitIcon = new CounterView(context);

            setIconValue(currentValue, false);

            limitIcon.setPadding(dp(24), dp(6), dp(24), dp(14));
            addView(limitIcon, LayoutHelper.createLinear(LayoutHelper.WRAP_CONTENT, LayoutHelper.WRAP_CONTENT, 0, Gravity.LEFT));
        }

        final FrameLayout defaultLayout = new FrameLayout(context);

        defaultText = new TextView(context);
        defaultText.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        defaultText.setText(LocaleController.getString("LimitFree", R.string.LimitFree));
        defaultText.setGravity(Gravity.CENTER_VERTICAL);
        defaultText.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));

        defaultCount = new TextView(context);
        defaultCount.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        defaultCount.setText(String.format("%d", premiumLimit));
        defaultCount.setGravity(Gravity.CENTER_VERTICAL);
        defaultCount.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));

        if (LocaleController.isRTL) {
            defaultLayout.addView(defaultText, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.RIGHT, 12, 0, 12, 0));
            defaultLayout.addView(defaultCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.LEFT, 12, 0, 12, 0));
        } else {
            defaultLayout.addView(defaultText, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.LEFT, 12, 0, 12, 0));
            defaultLayout.addView(defaultCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.RIGHT, 12, 0, 12, 0));
        }

        final FrameLayout premiumLayout = new FrameLayout(context);

        premiumText = new TextView(context);
        premiumText.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        premiumText.setText(LocaleController.getString("LimitPremium", R.string.LimitPremium));
        premiumText.setGravity(Gravity.CENTER_VERTICAL);
        premiumText.setTextColor(Color.WHITE);

        premiumCount = new TextView(context);
        premiumCount.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        premiumCount.setText(String.format("%d", premiumLimit));
        premiumCount.setGravity(Gravity.CENTER_VERTICAL);
        premiumCount.setTextColor(Color.WHITE);

        if (LocaleController.isRTL) {
            premiumLayout.addView(premiumText, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.RIGHT, 12, 0, 12, 0));
            premiumLayout.addView(premiumCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.LEFT, 12, 0, 12, 0));
        } else {
            premiumLayout.addView(premiumText, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.LEFT, 12, 0, 12, 0));
            premiumLayout.addView(premiumCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.RIGHT, 12, 0, 12, 0));
        }

        limitsContainer = new FrameLayout(context) {
>>>>>>> neko/main

            Paint grayPaint = new Paint();

            @Override
            protected void dispatchDraw(Canvas canvas) {
<<<<<<< HEAD
                grayPaint.setColor(Theme.getColor(Theme.key_windowBackgroundGray));
                AndroidUtilities.rectTmp.set(0, 0, getMeasuredWidth(), getMeasuredHeight());
                canvas.drawRoundRect(AndroidUtilities.rectTmp, AndroidUtilities.dp(6), AndroidUtilities.dp(6), grayPaint);

                canvas.save();
                canvas.clipRect(getMeasuredWidth() * percent, 0, getMeasuredWidth(), getMeasuredHeight());
=======
                if (isBoostsStyle) {
                    if (isStatistic) {
                        grayPaint.setColor(Theme.getColor(Theme.key_listSelector, resourcesProvider));
                    } else {
                        grayPaint.setColor(Theme.getColor(Theme.key_graySection, resourcesProvider));
                    }
                } else {
                    grayPaint.setColor(Theme.getColor(Theme.key_windowBackgroundGray, resourcesProvider));
                }
                AndroidUtilities.rectTmp.set(0, 0, getMeasuredWidth(), getMeasuredHeight());
                canvas.drawRoundRect(AndroidUtilities.rectTmp, dp(6), dp(6), grayPaint);

                canvas.save();
                if (!isBoostsStyle) {
                    canvas.clipRect(width1, 0, getMeasuredWidth(), getMeasuredHeight());
                }
>>>>>>> neko/main
                Paint paint = PremiumGradient.getInstance().getMainGradientPaint();
                if (parentVideForGradient != null) {
                    View parent = parentVideForGradient;
                    if (staticGradient != null) {
                        paint = staticGradient.paint;
                        staticGradient.gradientMatrixLinear(gradientTotalHeight, -gradientYOffset);
                    } else {
                        float y = 0;
                        View child = this;
                        while (child != parent) {
                            y += child.getY();
                            child = (View) child.getParent();
                        }
                        PremiumGradient.getInstance().updateMainGradientMatrix(0, 0, parent.getMeasuredWidth(), parent.getMeasuredHeight(), getGlobalXOffset() - getLeft(), -y);
                    }

                } else {
                    PremiumGradient.getInstance().updateMainGradientMatrix(0, 0, LimitPreviewView.this.getMeasuredWidth(), LimitPreviewView.this.getMeasuredHeight(), getGlobalXOffset() - getLeft(), -getTop());
                }
<<<<<<< HEAD
                canvas.drawRoundRect(AndroidUtilities.rectTmp, AndroidUtilities.dp(6), AndroidUtilities.dp(6), paint);
=======
                if (isBoostsStyle) {
                    AndroidUtilities.rectTmp.set(0, 0, width1, getMeasuredHeight());
                }
                canvas.drawRoundRect(AndroidUtilities.rectTmp, dp(6), dp(6), paint);
>>>>>>> neko/main
                canvas.restore();
                if (staticGradient == null) {
                    invalidate();
                }
                super.dispatchDraw(canvas);
            }
<<<<<<< HEAD
        };
        limitsContainer.setOrientation(LinearLayout.HORIZONTAL);

        FrameLayout limitLayout = new FrameLayout(context);

        TextView freeTextView = new TextView(context);
        freeTextView.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        freeTextView.setText(LocaleController.getString("LimitFree", R.string.LimitFree));
        freeTextView.setGravity(Gravity.CENTER_VERTICAL);
        freeTextView.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText));
        freeTextView.setPadding(AndroidUtilities.dp(12), 0, 0, 0);

        defaultCount = new TextView(context);
        defaultCount.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        defaultCount.setText(String.format("%d", premiumLimit));
        defaultCount.setGravity(Gravity.CENTER_VERTICAL);
        defaultCount.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText));

        if (percent > .3f) {
            if (LocaleController.isRTL) {
                limitLayout.addView(freeTextView, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.RIGHT, 36, 0, 12, 0));
                limitLayout.addView(defaultCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.LEFT, 12, 0, 0, 0));
            } else {
                limitLayout.addView(freeTextView, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.LEFT, 0, 0, 36, 0));
                limitLayout.addView(defaultCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.RIGHT, 0, 0, 12, 0));
            }
        }

        limitsContainer.addView(limitLayout, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 30, 2f * (1f - percent)));

        FrameLayout limitLayout2 = new FrameLayout(context);

        TextView limitTextView = new TextView(context);
        limitTextView.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        limitTextView.setText(LocaleController.getString("LimitPremium", R.string.LimitPremium));
        limitTextView.setGravity(Gravity.CENTER_VERTICAL);
        limitTextView.setTextColor(Color.WHITE);
        limitTextView.setPadding(AndroidUtilities.dp(12), 0, 0, 0);

        premiumCount = new TextView(context);
        premiumCount.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
        premiumCount.setText(String.format("%d", premiumLimit));
        premiumCount.setGravity(Gravity.CENTER_VERTICAL);
        premiumCount.setTextColor(Color.WHITE);

        if (percent < .7f) {
            if (LocaleController.isRTL) {
                limitLayout2.addView(limitTextView, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.RIGHT, 36, 0, 12, 0));
                limitLayout2.addView(premiumCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.LEFT, 12, 0, 0, 0));
            } else {
                limitLayout2.addView(limitTextView, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30, Gravity.LEFT, 0, 0, 36, 0));
                limitLayout2.addView(premiumCount, LayoutHelper.createFrame(LayoutHelper.WRAP_CONTENT, 30, Gravity.RIGHT, 0, 0, 12, 0));
            }
        }

        limitsContainer.addView(limitLayout2, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 30, 2f * percent));

        addView(limitsContainer, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, LayoutHelper.WRAP_CONTENT, 0, 0, 14, icon == 0 ? 0 : 12, 14, 0));
    }

    public void setIconValue(int currentValue) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append("d ").setSpan(new ColoredImageSpan(icon), 0, 1, 0);
        spannableStringBuilder.append(Integer.toString(currentValue));
        limitIcon.setText(spannableStringBuilder);
=======

            @Override
            protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                if (getChildCount() == 2) {
                    final int width = MeasureSpec.getSize(widthMeasureSpec);
                    final int height = MeasureSpec.getSize(heightMeasureSpec);
                    defaultLayout.measure(
                            MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                    );
                    final int minWidth1 = Math.max(defaultLayout.getMeasuredWidth(), dp(24) + defaultText.getMeasuredWidth() + (defaultCount.getVisibility() == View.VISIBLE ? dp(24) + defaultCount.getMeasuredWidth() : 0));
                    premiumLayout.measure(
                            MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                    );
                    if (isBoostsStyle) {
                        if (percent == 0) {
                            width1 = 0;
                            premiumCount.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));
                            defaultText.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));
                        } else if (percent < 1f) {
                            float leftWidth = defaultLayout.getMeasuredWidth() - AndroidUtilities.dp(8);
                            float rightWidth = premiumLayout.getMeasuredWidth() - AndroidUtilities.dp(8);
                            float availableWidth = width - leftWidth - rightWidth;
                            width1 = (int) (leftWidth + availableWidth * percent);
                            premiumCount.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));
                            defaultText.setTextColor(Color.WHITE);
                        } else {
                            width1 = width;
                            premiumCount.setTextColor(Color.WHITE);
                            defaultText.setTextColor(Color.WHITE);
                        }
                    } else {
                        final int minWidth2 = Math.max(premiumLayout.getMeasuredWidth(), dp(24) + premiumText.getMeasuredWidth() + (premiumCount.getVisibility() == View.VISIBLE ? dp(24) + premiumCount.getMeasuredWidth() : 0));
                        width1 = (int) Utilities.clamp(width * percent, width - minWidth2, minWidth1);
                        defaultLayout.measure(
                                MeasureSpec.makeMeasureSpec(width1, MeasureSpec.EXACTLY),
                                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                        );
                        premiumLayout.measure(
                                MeasureSpec.makeMeasureSpec(width - width1, MeasureSpec.EXACTLY),
                                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                        );
                    }
                    setMeasuredDimension(width, height);
                } else {
                    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                }
            }

            @Override
            protected void onLayout(boolean changed, int l, int t, int r, int b) {
                if (getChildCount() == 2) {
                    View child1 = getChildAt(0);
                    View child2 = getChildAt(1);
                    final int w = child1.getMeasuredWidth();
                    child1.layout(0, 0, w, b - t);
                    child2.layout(w, 0, r - l, b - t);
                } else {
                    super.onLayout(changed, l, t, r, b);
                }
            }
        };
        limitsContainer.addView(defaultLayout, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30));
        limitsContainer.addView(premiumLayout, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, 30));
        addView(limitsContainer, LayoutHelper.createLinear(LayoutHelper.MATCH_PARENT, 30, 0, 0, 14, icon == 0 ? 0 : 12, 14, 0));
    }

    public void setIconValue(int currentValue, boolean animated) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append("d ").setSpan(new ColoredImageSpan(icon), 0, 1, 0);
        spannableStringBuilder.append(Integer.toString(currentValue));
        limitIcon.setText(spannableStringBuilder, animated);
        limitIcon.requestLayout();
>>>>>>> neko/main
    }

    private float getGlobalXOffset() {
        return -LimitPreviewView.this.getMeasuredWidth() * 0.1f * progress - LimitPreviewView.this.getMeasuredWidth() * 0.2f;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (staticGradient == null) {
            if (inc) {
                progress += 16f / 1000f;
                if (progress > 3) {
                    inc = false;
                }
            } else {
                progress -= 16f / 1000f;
                if (progress < 1) {
                    inc = true;
                }
            }
            invalidate();
        }
        super.dispatchDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
<<<<<<< HEAD
        if (!wasAnimation && limitIcon != null && animationCanPlay && !premiumLocked) {
            int padding = AndroidUtilities.dp(14);
            float fromX = 0;
            float toX = padding + (getMeasuredWidth() - padding * 2) * position - limitIcon.getMeasuredWidth() / 2f;
            float fromProgressCenter = 0.5f;
            float toProgressCenter = 0.5f;
=======
        if (animateIncrease || (!wasAnimation && limitIcon != null && animationCanPlay && !premiumLocked)) {
            int padding = dp(14);
            boolean animateIncreaseFinal = animateIncrease;
            animateIncrease = false;
            float fromX = animateIncreaseFinal ? limitIcon.getTranslationX() : 0;
            float toX = padding + Math.max(width1, (getMeasuredWidth() - padding * 2) * position) - limitIcon.getMeasuredWidth() / 2f;
            float fromProgressCenter = 0.5f;
            float toProgressCenter = 0.5f;
            if (toX < padding) {
                toX = padding;
                fromProgressCenter = toProgressCenter = 0f;
            }
>>>>>>> neko/main
            if (toX > getMeasuredWidth() - padding - limitIcon.getMeasuredWidth()) {
                toX = getMeasuredWidth() - padding - limitIcon.getMeasuredWidth();
                toProgressCenter = 1f;
            }
            limitIcon.setAlpha(1f);
            limitIcon.setTranslationX(fromX);
            limitIcon.setPivotX(limitIcon.getMeasuredWidth() / 2f);
            limitIcon.setPivotY(limitIcon.getMeasuredHeight());
<<<<<<< HEAD
            limitIcon.setScaleX(0);
            limitIcon.setScaleY(0);
            limitIcon.createAnimationLayouts();
=======
            if (!animateIncreaseFinal) {
                limitIcon.setScaleX(0);
                limitIcon.setScaleY(0);
                limitIcon.createAnimationLayouts();
            }
>>>>>>> neko/main

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1f);
            float finalToX = toX;
            float finalToProgressCenter = toProgressCenter;
<<<<<<< HEAD
=======
            float toWidth = width1;
            if (animateIncreaseFinal) {
                width1 = animateIncreaseWidth;
            }
            float finalFromProgressCenter = fromProgressCenter;
>>>>>>> neko/main
            valueAnimator.addUpdateListener(animation -> {
                float v = (float) animation.getAnimatedValue();
                float moveValue = Math.min(1f, v);
                if (v > 1f) {
                    if (!wasHaptic) {
                        wasHaptic = true;
                        limitIcon.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                    }
<<<<<<< HEAD
                    limitIcon.setRotation((v - 1f) * 60);
                } else {
                    limitIcon.setRotation(0);
                }
                limitIcon.setTranslationX(fromX * (1f - moveValue) + finalToX * moveValue);
                float arrowCenter = fromProgressCenter * (1f - moveValue) + finalToProgressCenter * moveValue;
                limitIcon.setArrowCenter(arrowCenter);
                float scale = Math.min(1, moveValue * 2f);
                limitIcon.setScaleX(scale);
                limitIcon.setScaleY(scale);
=======
                    limitIcon.setRotation(limitIconRotation + (v - 1f) * 60);
                } else {
                    limitIcon.setRotation(limitIconRotation);
                }
                limitIcon.setTranslationX(fromX * (1f - moveValue) + finalToX * moveValue);
                float arrowCenter = finalFromProgressCenter * (1f - moveValue) + finalToProgressCenter * moveValue;
                limitIcon.setArrowCenter(arrowCenter);
                float scale = Math.min(1, moveValue * 2f);
                if (!animateIncreaseFinal) {
                    limitIcon.setScaleX(scale);
                    limitIcon.setScaleY(scale);
                } else {
                    width1 = (int) AndroidUtilities.lerp(animateIncreaseWidth, toWidth, moveValue);
                    limitsContainer.invalidate();
                }
>>>>>>> neko/main
                limitIcon.setPivotX(limitIcon.getMeasuredWidth() * arrowCenter);
            });

            valueAnimator.setInterpolator(new OvershootInterpolator());
<<<<<<< HEAD
            valueAnimator.setDuration(1000);
            valueAnimator.setStartDelay(200);
            valueAnimator.start();

            wasAnimation = true;
        } else if (premiumLocked) {
            int padding = AndroidUtilities.dp(14);
=======
            if (animateIncreaseFinal) {
                ValueAnimator valueAnimator1 = ValueAnimator.ofFloat(0, 1f);
                valueAnimator1.addUpdateListener(animation -> {
                    float p = (float) animation.getAnimatedValue();
                    float k = 0.5f;
                    float angle = -7;
                    limitIconRotation = p < k ? p / k * angle : angle * (1f - (p - k) / (1f - k));
                });
                valueAnimator1.setDuration(500);
                valueAnimator1.start();
                valueAnimator.setDuration(600);
            } else {
                valueAnimator.setDuration(1000);
                valueAnimator.setStartDelay(200);
            }
            valueAnimator.start();

            wasAnimation = true;
        } else if (isBoostsStyle) {
            limitIcon.setAlpha(1f);
            limitIcon.setScaleX(1f);
            limitIcon.setScaleY(1f);
        } else if (premiumLocked) {
            int padding = dp(14);
>>>>>>> neko/main
            float toX = padding + (getMeasuredWidth() - padding * 2) * 0.5f - limitIcon.getMeasuredWidth() / 2f;
            if (!wasAnimation && animationCanPlay) {
                wasAnimation = true;
                limitIcon.animate().alpha(1).scaleX(1).scaleY(1).setDuration(200).setInterpolator(new OvershootInterpolator()).start();
            } else if (!wasAnimation) {
                limitIcon.setAlpha(0);
                limitIcon.setScaleX(0);
                limitIcon.setScaleY(0);
            } else {
                limitIcon.setAlpha(1f);
                limitIcon.setScaleX(1f);
                limitIcon.setScaleY(1f);
            }
            limitIcon.setTranslationX(toX);
<<<<<<< HEAD
        } else if (limitIcon != null){
=======
        } else if (limitIcon != null) {
>>>>>>> neko/main
            limitIcon.setAlpha(0);
        }
    }

    public void setType(int type) {
        if (type == LimitReachedBottomSheet.TYPE_LARGE_FILE) {
            if (limitIcon != null) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                spannableStringBuilder.append("d ").setSpan(new ColoredImageSpan(icon), 0, 1, 0);
                spannableStringBuilder.append(UserConfig.getInstance(UserConfig.selectedAccount).isPremium() ? "4 GB" : "2 GB");
<<<<<<< HEAD
                limitIcon.setText(spannableStringBuilder);
=======
                limitIcon.setText(spannableStringBuilder, false);
>>>>>>> neko/main
            }
            premiumCount.setText("4 GB");
        } else if (type == LimitReachedBottomSheet.TYPE_ADD_MEMBERS_RESTRICTED) {
            if (limitIcon != null) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                spannableStringBuilder.append("d").setSpan(new ColoredImageSpan(icon), 0, 1, 0);
<<<<<<< HEAD
                limitIcon.setText(spannableStringBuilder);
=======
                limitIcon.setText(spannableStringBuilder, false);
>>>>>>> neko/main
            }
            premiumCount.setText("");
        }
    }

    public void setBagePosition(float position) {
        this.position = MathUtils.clamp(position, 0.1f, 0.9f);
    }

    public void setParentViewForGradien(ViewGroup containerView) {
        parentVideForGradient = containerView;
    }

    public void setStaticGradinet(PremiumGradient.PremiumGradientTools gradientTools) {
        staticGradient = gradientTools;
    }

    public void setDelayedAnimation() {
        animationCanPlay = false;
    }

    public void startDelayedAnimation() {
        animationCanPlay = true;
        requestLayout();
    }

    public void setPremiumLocked() {
        limitsContainer.setVisibility(View.GONE);
<<<<<<< HEAD
        limitIcon.setPadding(AndroidUtilities.dp(24), AndroidUtilities.dp(3), AndroidUtilities.dp(24), AndroidUtilities.dp(3));
        premiumLocked = true;
    }

    private class CounterView extends View {

        Path path = new Path();
        PathEffect pathEffect = new CornerPathEffect(AndroidUtilities.dp(6));
=======
        limitIcon.setPadding(dp(24), dp(3), dp(24), dp(3));
        premiumLocked = true;
    }

    public void setBoosts(TLRPC.TL_stories_boostsStatus boosts, boolean boosted) {
        int k = boosts.current_level_boosts;
        boolean isZeroLevelBoosts = boosts.current_level_boosts == boosts.boosts;
        if ((isZeroLevelBoosts && boosted) || boosts.next_level_boosts == 0) {
            percent = 1f;
            defaultText.setText(LocaleController.formatString("BoostsLevel", R.string.BoostsLevel, boosts.level - 1));
            premiumCount.setText(LocaleController.formatString("BoostsLevel", R.string.BoostsLevel, boosts.level));
        } else {
            percent = MathUtils.clamp((boosts.boosts - k) / (float) (boosts.next_level_boosts - k), 0, 1f);
            defaultText.setText(LocaleController.formatString("BoostsLevel", R.string.BoostsLevel, boosts.level));
            premiumCount.setText(LocaleController.formatString("BoostsLevel", R.string.BoostsLevel, boosts.level + 1));
        }
        ((FrameLayout.LayoutParams) premiumCount.getLayoutParams()).gravity = Gravity.RIGHT;
        setType(LimitReachedBottomSheet.TYPE_BOOSTS);
        defaultCount.setVisibility(View.GONE);
        premiumText.setVisibility(View.GONE);

        premiumCount.setTextColor(Theme.getColor(Theme.key_windowBackgroundWhiteBlackText, resourcesProvider));
        defaultText.setTextColor(Color.WHITE);


        setIconValue(boosts.boosts, false);
        isBoostsStyle = true;
    }

    public void increaseCurrentValue(int value, int maxValue) {
        currentValue++;
        percent = MathUtils.clamp(value / (float) maxValue, 0f, 1f);
        animateIncrease = true;
        animateIncreaseWidth = width1;

        setIconValue(currentValue, true);
        limitsContainer.requestLayout();
        requestLayout();
    }

    private class CounterView extends View {

        Path path = new Path();
        PathEffect pathEffect = new CornerPathEffect(dp(6));
>>>>>>> neko/main
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

        StaticLayout textLayout;
        float textWidth;
        CharSequence text;

        ArrayList<AnimatedLayout> animatedLayouts = new ArrayList<AnimatedLayout>();
        StaticLayout animatedStableLayout;
        boolean animationInProgress;

        float arrowCenter;
        boolean invalidatePath;

        public CounterView(Context context) {
            super(context);
            textPaint.setTypeface(AndroidUtilities.getTypeface("fonts/rmedium.ttf"));
<<<<<<< HEAD
            textPaint.setTextSize(AndroidUtilities.dp(22));
=======
            textPaint.setTextSize(dp(22));
>>>>>>> neko/main
            textPaint.setColor(Color.WHITE);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            textWidth = textPaint.measureText(text, 0, text.length());
<<<<<<< HEAD
            textLayout = new StaticLayout(text, textPaint, (int) textWidth + AndroidUtilities.dp(12), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            setMeasuredDimension((int) (textWidth + getPaddingRight() + getPaddingLeft()), AndroidUtilities.dp(44) + AndroidUtilities.dp(8));
=======
            textLayout = new StaticLayout(text, textPaint, (int) textWidth + dp(12), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            setMeasuredDimension((int) (textWidth + getPaddingRight() + getPaddingLeft()), dp(44) + dp(8));
>>>>>>> neko/main
            updatePath();
        }

        private void updatePath() {
<<<<<<< HEAD
            int h = getMeasuredHeight() - AndroidUtilities.dp(8);
            float widthHalf = getMeasuredWidth() * arrowCenter;
            float x2 = Utilities.clamp(widthHalf + AndroidUtilities.dp(8), getMeasuredWidth(), 0);
            float x3 =  Utilities.clamp(widthHalf + AndroidUtilities.dp(10), getMeasuredWidth(), 0);

            path.rewind();
            path.moveTo(widthHalf - AndroidUtilities.dp(24), h - h / 2f - AndroidUtilities.dp(2));
            path.lineTo(widthHalf - AndroidUtilities.dp(24), h);
            path.lineTo(widthHalf - AndroidUtilities.dp(8), h);
            path.lineTo(widthHalf, h + AndroidUtilities.dp(8));
=======
            int h = getMeasuredHeight() - dp(8);
            float widthHalf = getMeasuredWidth() * arrowCenter;
            float x2 = Utilities.clamp(widthHalf + dp(8), getMeasuredWidth(), 0);
            float x3 = Utilities.clamp(widthHalf + dp(10), getMeasuredWidth(), AndroidUtilities.dp(24));
            float x4 = Utilities.clamp(widthHalf - dp(24), getMeasuredWidth(), 0);
            float x5 = Utilities.clamp(widthHalf - dp(8), getMeasuredWidth(), 0);

            path.rewind();
            path.moveTo(x4, h - h / 2f - dp(2));
            path.lineTo(x4, h);
            path.lineTo(x5, h);
            path.lineTo(widthHalf, h + dp(8));
>>>>>>> neko/main
            if (arrowCenter < 0.7f) {
                path.lineTo(x2, h);
            }
            path.lineTo(x3, h);
<<<<<<< HEAD
            path.lineTo(x3, h - h / 2f - AndroidUtilities.dp(2));
=======
            path.lineTo(x3, h - h / 2f - dp(2));
>>>>>>> neko/main
            path.close();
        }

        @Override
        protected void onDraw(Canvas canvas) {
<<<<<<< HEAD
            int h = getMeasuredHeight() - AndroidUtilities.dp(8);
            if (premiumLocked) {
                h = getMeasuredHeight();
                PremiumGradient.getInstance().updateMainGradientMatrix(0, 0, LimitPreviewView.this.getMeasuredWidth(), LimitPreviewView.this.getMeasuredHeight(), getGlobalXOffset() - getX(), -getTop());
                AndroidUtilities.rectTmp.set(0, AndroidUtilities.dp(3), getMeasuredWidth(), h - AndroidUtilities.dp(3));
=======
            int h = getMeasuredHeight() - dp(8);
            if (premiumLocked) {
                h = getMeasuredHeight();
                PremiumGradient.getInstance().updateMainGradientMatrix(0, 0, LimitPreviewView.this.getMeasuredWidth(), LimitPreviewView.this.getMeasuredHeight(), getGlobalXOffset() - getX(), -getTop());
                AndroidUtilities.rectTmp.set(0, dp(3), getMeasuredWidth(), h - dp(3));
>>>>>>> neko/main
                canvas.drawRoundRect(AndroidUtilities.rectTmp, h / 2f, h / 2f, PremiumGradient.getInstance().getPremiumLocakedPaint());
            } else {
                if (invalidatePath) {
                    invalidatePath = false;
                    updatePath();
                }
                PremiumGradient.getInstance().updateMainGradientMatrix(0, 0, LimitPreviewView.this.getMeasuredWidth(), LimitPreviewView.this.getMeasuredHeight(), getGlobalXOffset() - getX(), -getTop());
                AndroidUtilities.rectTmp.set(0, 0, getMeasuredWidth(), h);
                canvas.drawRoundRect(AndroidUtilities.rectTmp, h / 2f, h / 2f, PremiumGradient.getInstance().getMainGradientPaint());
                PremiumGradient.getInstance().getMainGradientPaint().setPathEffect(pathEffect);
                canvas.drawPath(path, PremiumGradient.getInstance().getMainGradientPaint());
                PremiumGradient.getInstance().getMainGradientPaint().setPathEffect(null);
                invalidate();
            }

            float x = (getMeasuredWidth() - textLayout.getWidth()) / 2f;
            float y = (h - textLayout.getHeight()) / 2f;
            if (!animationInProgress) {
                if (textLayout != null) {
                    canvas.save();
                    canvas.translate(x, y);
                    textLayout.draw(canvas);
                    canvas.restore();
                }
            } else {
                canvas.save();
<<<<<<< HEAD
                canvas.clipRect(0, 0, getMeasuredWidth(), getMeasuredHeight() - AndroidUtilities.dp(8));
=======
                canvas.clipRect(0, 0, getMeasuredWidth(), getMeasuredHeight() - dp(8));
>>>>>>> neko/main
                if (animatedStableLayout != null) {
                    canvas.save();
                    canvas.translate(x, y);
                    animatedStableLayout.draw(canvas);
                    canvas.restore();
                }
                for (int i = 0; i < animatedLayouts.size(); i++) {
                    AnimatedLayout animatedLayout = animatedLayouts.get(i);
                    canvas.save();
<<<<<<< HEAD
                    if (animatedLayout.direction) {
=======
                    if (animatedLayout.replace) {
                        canvas.translate(x + animatedLayout.x, y + h * (animatedLayout.progress) - h * (1 - animatedLayout.staticLayouts.size()));
                        for (int j = 0; j < animatedLayout.staticLayouts.size(); j++) {
                            canvas.translate(0, -h);
                            animatedLayout.staticLayouts.get(j).draw(canvas);
                        }
                    } else if (animatedLayout.direction) {
>>>>>>> neko/main
                        canvas.translate(x + animatedLayout.x, y - h * 10 * animatedLayout.progress + h * (10 - animatedLayout.staticLayouts.size()));
                        for (int j = 0; j < animatedLayout.staticLayouts.size(); j++) {
                            canvas.translate(0, h);
                            animatedLayout.staticLayouts.get(j).draw(canvas);
                        }
                    } else {
                        canvas.translate(x + animatedLayout.x, y + h * 10 * animatedLayout.progress - h * (10 - animatedLayout.staticLayouts.size()));
                        for (int j = 0; j < animatedLayout.staticLayouts.size(); j++) {
                            canvas.translate(0, -h);
                            animatedLayout.staticLayouts.get(j).draw(canvas);
                        }
                    }
                    canvas.restore();
                }

                canvas.restore();
            }
        }

        @Override
        public void setTranslationX(float translationX) {
            if (translationX != getTranslationX()) {
                super.setTranslationX(translationX);
                invalidate();
            }
        }

        void createAnimationLayouts() {
            animatedLayouts.clear();
<<<<<<< HEAD
=======
            if (isBoostsStyle && currentValue == 0) {
                return;
            }
>>>>>>> neko/main
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);

            boolean direction = true;
            int directionCount = 0;
            for (int i = 0; i < text.length(); i++) {
                if (Character.isDigit(text.charAt(i))) {
                    AnimatedLayout animatedLayout = new AnimatedLayout();
                    animatedLayouts.add(animatedLayout);
                    animatedLayout.x = textLayout.getSecondaryHorizontal(i);
                    animatedLayout.direction = direction;
                    if (directionCount >= 1) {
                        direction = !direction;
                        directionCount = 0;
                    }
                    directionCount++;

                    int digit = text.charAt(i) - '0';
                    if (digit == 0) {
                        digit = 10;
                    }
                    for (int j = 1; j <= digit; j++) {
                        int k = j;
                        if (k == 10) {
                            k = 0;
                        }
                        String str = "" + k;
                        StaticLayout staticLayout = new StaticLayout(str, textPaint, (int) textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                        animatedLayout.staticLayouts.add(staticLayout);
                    }
                    spannableStringBuilder.setSpan(new EmptyStubSpan(), i, i + 1, 0);
                }
            }
<<<<<<< HEAD
            animatedStableLayout = new StaticLayout(spannableStringBuilder, textPaint, (int) textWidth + AndroidUtilities.dp(12), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
=======
            animatedStableLayout = new StaticLayout(spannableStringBuilder, textPaint, (int) textWidth + dp(12), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
>>>>>>> neko/main
            for (int i = 0; i < animatedLayouts.size(); i++) {
                animationInProgress = true;
                AnimatedLayout layout = animatedLayouts.get(i);
                layout.valueAnimator = ValueAnimator.ofFloat(0, 1f);
                layout.valueAnimator.addUpdateListener(animation -> {
                    layout.progress = (float) animation.getAnimatedValue();
                    invalidate();
                });
                layout.valueAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        layout.valueAnimator = null;
                        checkAnimationComplete();
                    }
                });
                layout.valueAnimator.setInterpolator(CubicBezierInterpolator.EASE_OUT);
                layout.valueAnimator.setDuration(750);
                layout.valueAnimator.setStartDelay((animatedLayouts.size() - 1 - i) * 60L);
                layout.valueAnimator.start();
            }
        }

<<<<<<< HEAD
=======

        void createAnimationLayoutsDiff(CharSequence oldText) {
            animatedLayouts.clear();
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
            int directionCount = 0;
            for (int i = text.length() - 1; i >= 0; i--) {
                char oldChar = i < oldText.length() ? oldText.charAt(i) : ' ';
                if (oldChar != text.charAt(i) && Character.isDigit(text.charAt(i))) {
                    AnimatedLayout animatedLayout = new AnimatedLayout();
                    animatedLayouts.add(animatedLayout);
                    animatedLayout.x = textLayout.getSecondaryHorizontal(i);
                    animatedLayout.replace = true;
                    if (directionCount >= 1) {
                        directionCount = 0;
                    }
                    directionCount++;

                    StaticLayout staticLayoutOld = new StaticLayout("" + oldChar, textPaint, (int) textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    animatedLayout.staticLayouts.add(staticLayoutOld);

                    StaticLayout staticLayout = new StaticLayout("" + text.charAt(i), textPaint, (int) textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    animatedLayout.staticLayouts.add(staticLayout);
                    spannableStringBuilder.setSpan(new EmptyStubSpan(), i, i + 1, 0);
                }
            }
            animatedStableLayout = new StaticLayout(spannableStringBuilder, textPaint, (int) textWidth + dp(12), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            for (int i = 0; i < animatedLayouts.size(); i++) {
                animationInProgress = true;
                AnimatedLayout layout = animatedLayouts.get(i);
                layout.valueAnimator = ValueAnimator.ofFloat(0, 1f);
                layout.valueAnimator.addUpdateListener(animation -> {
                    layout.progress = (float) animation.getAnimatedValue();
                    invalidate();
                });
                layout.valueAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        layout.valueAnimator = null;
                        checkAnimationComplete();
                    }
                });
                layout.valueAnimator.setInterpolator(CubicBezierInterpolator.EASE_OUT);
                layout.valueAnimator.setDuration(250);
                layout.valueAnimator.setStartDelay((animatedLayouts.size() - 1 - i) * 60L);
                layout.valueAnimator.start();
            }
        }


>>>>>>> neko/main
        private void checkAnimationComplete() {
            for (int i = 0; i < animatedLayouts.size(); i++) {
                if (animatedLayouts.get(i).valueAnimator != null) {
                    return;
                }
            }
            animatedLayouts.clear();
            animationInProgress = false;
            invalidate();
        }

<<<<<<< HEAD
        public void setText(CharSequence text) {
            this.text = text;
=======
        public void setText(CharSequence text, boolean animated) {
            if (!animated) {
                this.text = text;
            } else {
                CharSequence oldText = this.text;
                this.text = text;
                createAnimationLayoutsDiff(oldText);
            }
>>>>>>> neko/main
        }

        public void setArrowCenter(float v) {
            if (arrowCenter != v) {
                arrowCenter = v;
                invalidatePath = true;
                invalidate();
            }
        }

        private class AnimatedLayout {
<<<<<<< HEAD
=======
            public boolean replace;
>>>>>>> neko/main
            ArrayList<StaticLayout> staticLayouts = new ArrayList<>();
            float progress;
            public boolean direction;
            float x;
            ValueAnimator valueAnimator;
        }
    }
}
