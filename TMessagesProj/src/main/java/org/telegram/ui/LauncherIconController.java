package org.telegram.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
<<<<<<< HEAD
import android.os.Build;

import com.exteragram.messenger.utils.AppUtils;

import org.telegram.messenger.ApplicationLoader;
import org.telegram.messenger.BuildVars;
=======

import org.telegram.messenger.ApplicationLoader;
>>>>>>> neko/main
import org.telegram.messenger.R;

public class LauncherIconController {
    public static void tryFixLauncherIconIfNeeded() {
        for (LauncherIcon icon : LauncherIcon.values()) {
            if (isEnabled(icon)) {
<<<<<<< HEAD
                if (icon == LauncherIcon.MONET) {
                    setIcon(LauncherIcon.DEFAULT);
                    setIcon(LauncherIcon.MONET);
                }
                return;
            }
        }
=======
                return;
            }
        }

>>>>>>> neko/main
        setIcon(LauncherIcon.DEFAULT);
    }

    public static boolean isEnabled(LauncherIcon icon) {
        Context ctx = ApplicationLoader.applicationContext;
        int i = ctx.getPackageManager().getComponentEnabledSetting(icon.getComponentName(ctx));
        return i == PackageManager.COMPONENT_ENABLED_STATE_ENABLED || i == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT && icon == LauncherIcon.DEFAULT;
    }

    public static void setIcon(LauncherIcon icon) {
        Context ctx = ApplicationLoader.applicationContext;
        PackageManager pm = ctx.getPackageManager();
        for (LauncherIcon i : LauncherIcon.values()) {
            pm.setComponentEnabledSetting(i.getComponentName(ctx), i == icon ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED :
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        }
    }

    public enum LauncherIcon {
<<<<<<< HEAD
        DEFAULT("DefaultIcon", BuildVars.isBetaApp() ? R.mipmap.ic_background_beta : R.color.ic_background, BuildVars.isBetaApp() ? R.mipmap.ic_foreground_beta : R.drawable.ic_foreground, R.string.AppIconDefault),
        MONET("MonetIcon", R.color.ic_background_monet, R.drawable.ic_foreground_monet, R.string.AppIconMonet, Build.VERSION.SDK_INT < 31 || Build.VERSION.SDK_INT > 32),
        GRADIENT("GradientIcon", R.mipmap.ic_background_gradient, R.drawable.ic_foreground_white, R.string.AppIconGradient),
        AURORA("AuroraIcon", R.mipmap.ic_background_aurora, R.drawable.ic_foreground_white, R.string.AppIconAurora),
        NEO("NeoIcon", R.mipmap.ic_background_neo, R.mipmap.ic_foreground_neo, R.string.AppIconNeo),
        GOOGLE("GoogleIcon", R.color.white, R.mipmap.ic_foreground_google, R.string.AppIconGoogle),
        AMETHYST("AmethystIcon", R.mipmap.ic_background_amethyst, R.mipmap.ic_foreground_amethyst, R.string.AppIconAmethyst),
        DSGN480("Dsgn480Icon", R.mipmap.ic_background_480dsgn, R.mipmap.ic_foreground_480dsgn, R.string.AppIcon480DSGN),
        ORBIT("OrbitIcon", R.color.ic_background, R.mipmap.ic_foreground_orbit, R.string.AppIconOrbit),
        SPACE("SpaceIcon", R.mipmap.ic_background_space, R.mipmap.ic_foreground_space, R.string.AppIconSpace),
        WINTER("WinterIcon", R.mipmap.ic_background_winter, R.drawable.ic_foreground, R.string.AppIconWinter, !AppUtils.isWinter()),
        SUS("SusIcon", R.color.ic_background_sus, R.mipmap.ic_foreground_sus, R.string.AppIconSus);
=======
        DEFAULT("DefaultIcon", R.color.ic_launcher_background, R.drawable.ic_launcher_foreground, R.string.AppIconDefault),
        //VINTAGE("VintageIcon", R.drawable.icon_6_background_sa, R.drawable.ic_launcher_foreground, R.string.AppIconVintage),
        AQUA("AquaIcon", R.drawable.icon_4_background_sa, R.drawable.ic_launcher_foreground, R.string.AppIconAqua),
        PREMIUM("PremiumIcon", R.drawable.icon_3_background_sa, R.drawable.ic_launcher_foreground, R.string.AppIconPremium),
        TURBO("TurboIcon", R.drawable.icon_5_background_sa, R.drawable.ic_launcher_foreground, R.string.AppIconTurbo),
        NOX("NoxIcon", R.drawable.icon_2_background_sa, R.drawable.ic_launcher_foreground, R.string.AppIconNox),
        MERIO("MerioIcon", R.mipmap.icon_7_launcher_background, R.mipmap.icon_7_launcher_foreground, R.string.AppIconMerio),
        RAINBOW("RainbowIcon", R.color.icon_8_launcher_background, R.mipmap.icon_8_launcher_foreground, R.string.AppIconRainbow),
        SCHOOL("OldSchoolIcon", R.mipmap.icon_9_launcher_background, R.mipmap.icon_9_launcher_foreground, R.string.AppIconOldSchool),
        MUSHEEN("MusheenIcon", R.color.ic_launcher_background, R.mipmap.icon_10_launcher_foreground, R.string.AppIconMusheen),
        SPACE("SpaceIcon", R.mipmap.icon_11_launcher_background, R.mipmap.icon_11_launcher_foreground, R.string.AppIconSpace),
        CLOUD("CloudIcon", R.color.ic_launcher_background, R.mipmap.icon_12_launcher_foreground, R.string.AppIconCloud),
        NEON("NeonIcon", R.mipmap.icon_13_launcher_background, R.mipmap.icon_13_launcher_foreground, R.string.AppIconNeon),
        MATERIAL("MaterialIcon", R.mipmap.icon_14_launcher_background, R.mipmap.icon_14_launcher_foreground, R.string.AppIconMaterial);
>>>>>>> neko/main

        public final String key;
        public final int background;
        public final int foreground;
        public final int title;
        public final boolean premium;
<<<<<<< HEAD
        public final boolean hidden;
=======
>>>>>>> neko/main

        private ComponentName componentName;

        public ComponentName getComponentName(Context ctx) {
            if (componentName == null) {
<<<<<<< HEAD
                componentName = new ComponentName(ctx.getPackageName(), "com.exteragram.messenger." + key);
=======
                componentName = new ComponentName(ctx.getPackageName(), "org.telegram.messenger." + key);
>>>>>>> neko/main
            }
            return componentName;
        }

        LauncherIcon(String key, int background, int foreground, int title) {
            this(key, background, foreground, title, false);
        }

<<<<<<< HEAD
        LauncherIcon(String key, int background, int foreground, int title, boolean hidden) {
=======
        LauncherIcon(String key, int background, int foreground, int title, boolean premium) {
>>>>>>> neko/main
            this.key = key;
            this.background = background;
            this.foreground = foreground;
            this.title = title;
<<<<<<< HEAD
            this.premium = false;
            this.hidden = hidden;
=======
            this.premium = premium;
>>>>>>> neko/main
        }
    }
}
